exe: shell.o
	gcc -o exe shell.o
shell.o: shell.c 
	gcc -c shell.c 

clean:
	-@rm -f shell.o

install: myshell
		cp shell /usr/bin && \
		chmod a+x /usr/bin/shell && \
		chmod og-w /usr/bin/shell && \